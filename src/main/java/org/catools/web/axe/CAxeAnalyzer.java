package org.catools.web.axe;

import com.deque.axe.AXE;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.catools.common.io.CResource;
import org.catools.common.utils.CJsonUtil;
import org.catools.web.axe.entities.CAxePage;
import org.openqa.selenium.WebDriver;

import java.net.URL;

@Data
@NoArgsConstructor
public class CAxeAnalyzer {
    private static final URL AXE_MIN_JS = CResource.getResource("axe.min.js", CAxeAnalyzer.class);

    public static CAxePage analyzePage(final WebDriver driver) {
        CAxePage page = CJsonUtil.read(getAxe(driver).analyze().toString(), CAxePage.class);
        page.setTitle(driver.getTitle());
        return page;
    }

    private static AXE.Builder getAxe(WebDriver driver) {
        return new AXE.Builder(driver, AXE_MIN_JS);
    }
}
