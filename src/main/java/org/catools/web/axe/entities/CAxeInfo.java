package org.catools.web.axe.entities;

import lombok.Data;

@Data
public class CAxeInfo {
    private String id;
    private String message;
    private String impact;
}
